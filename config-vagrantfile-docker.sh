#!/bin/bash

set -e

DISTRO=$1
SUITE=$2
ABBREV_DEST=$3
VAGRANT_BOX_NAME_SOURCE=$4
LOCAL_BOX_FILE_SOURCE=$5

cp Vagrantfile-template Vagrantfile

sed -i.bak "s|<VAGRANT_BOX_NAME_SOURCE>|$VAGRANT_BOX_NAME_SOURCE|g" Vagrantfile
sed -i.bak "s|<PWD>|$PWD|g" Vagrantfile
sed -i.bak "s|<DISTRO>|$DISTRO|g" Vagrantfile
sed -i.bak "s|<SUITE>|$SUITE|g" Vagrantfile
sed -i.bak "s|<LOCAL_BOX_FILE_SOURCE>|$LOCAL_BOX_FILE_SOURCE|g" Vagrantfile
sed -i.bak "s|<ABBREV_DEST>|$ABBREV_DEST|g" Vagrantfile
rm Vagrantfile.bak
