#!/bin/bash

set -e

DEBIAN_VERSION='10.0.0'
DISTRO='debian'
SUITE='buster'
ABBREV='min-stage0'
BOX_NAME="$DISTRO-$SUITE-$ABBREV"
BOX_FILE="$BOX_NAME.box"
VIRTUALBOX_NAME="packer-$BOX_NAME"
VIRTUALBOX_DIR="/home/`whoami`/VirtualBox VMs/packer-$DISTRO-$SUITE-$ABBREV"

if [[ -f $BOX_FILE ]] ; then
  echo '------------'
  echo "rm $BOX_FILE"
  rm $BOX_FILE
fi

if [ -d 'output-virtualbox-iso' ]; then
  echo '------------------------------'
  echo "rm -Rf 'output-virtualbox-iso'"
  rm -Rf 'output-virtualbox-iso'
fi

if [ -d "$VIRTUALBOX_DIR" ]; then
  echo '-----------------------'
  echo 'rm -rf "$VIRTUALBOX_DIR"'
  rm -rf "$VIRTUALBOX_DIR"
fi

cp packer-template.json packer.json

sed -i.bak "s|<DEBIAN_VERSION>|$DEBIAN_VERSION|g" packer.json
sed -i.bak "s|<BOX_FILE>|$BOX_FILE|g" packer.json
sed -i.bak "s|<VIRTUALBOX_NAME>|$VIRTUALBOX_NAME|g" packer.json
rm packer.json.bak

echo '-----------------------------'
echo 'time packer build packer.json'
time packer build packer.json
echo ' / \ '
echo '/ | \'
echo '  |  '
echo 'Time used to build Vagrant box file'
