#!/bin/bash

set -e

OWNER=$1
DISTRO=$2
SUITE=$3
ABBREV_DEST=$4
ABBREV_SOURCE=$5

VAGRANT_BOX_NAME_DEST="$DISTRO-$SUITE-$ABBREV_DEST"
VAGRANT_BOX_NAME_SOURCE="$DISTRO-$SUITE-$ABBREV_SOURCE"
LOCAL_BOX_FILE_DEST="$VAGRANT_BOX_NAME_DEST.box"
LOCAL_BOX_FILE_SOURCE="$VAGRANT_BOX_NAME_SOURCE.box"
VAGRANT_DIR="/home/`whoami`/.vagrant.d/boxes/$VAGRANT_BOX_NAME_SOURCE"

# NOTE: The virtual machine started up when executing "vagrant up"
# automatically gets the same name as the parent directory of the
# Vagrantfile.
VIRTUALBOX_NAME=`basename "$PWD"`

bash config-vagrantfile-docker.sh $DISTRO $SUITE $ABBREV_DEST $VAGRANT_BOX_NAME_SOURCE $LOCAL_BOX_FILE_SOURCE

bash clean.sh $VAGRANT_BOX_NAME_SOURCE $LOCAL_BOX_FILE_DEST $VIRTUALBOX_NAME

bash git-clone-docker.sh "$OWNER" "$DISTRO" "$SUITE" "$ABBREV_DEST"

echo '**********************************'
echo 'BEGIN: time vagrant up --provision'
echo '**********************************'
time vagrant up --provision
echo ' /|\'
echo '/ | \'
echo '  |'
echo 'Time used to provision the Vagrant box'
echo '*************************************'
echo 'FINISHED: time vagrant up --provision'
echo '*************************************'

echo '--------------------------------------------------'
echo "time vagrant package --output $LOCAL_BOX_FILE_DEST"
time vagrant package --output $LOCAL_BOX_FILE_DEST
echo ' /|\'
echo '/ | \'
echo '  |'
echo 'Time used to package the Vagrant box'
