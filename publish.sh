#!/bin/bash

ABBREV=$1

mkdir -p log
DATE=`date +%Y%m%d-%H%M-%S`
bash exec-publish.sh $ABBREV 2>&1 | tee log/log-$ABBREV-$DATE.txt
