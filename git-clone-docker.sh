#!/bin/bash

OWNER=$1
DISTRO=$2
SUITE=$3
ABBREV=$4

rm -rf docker-$DISTRO-$SUITE-$ABBREV
wait
git clone https://gitlab.com/$OWNER/docker-$DISTRO-$SUITE-$ABBREV.git
