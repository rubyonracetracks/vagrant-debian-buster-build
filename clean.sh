#!/bin/bash

VAGRANT_BOX_NAME_SOURCE=$1
LOCAL_BOX_FILE_DEST=$2
VIRTUALBOX_NAME=$3

echo '*********************************'
echo 'BEGIN: pre-build cleaning process'
echo '*********************************'

echo '---------------'
echo 'vagrant halt -f'
vagrant halt -f

echo '------------------'
echo 'vagrant destroy -f'
vagrant destroy -f

echo '------------------------------------------------'
echo "vagrant box list | grep $VAGRANT_BOX_NAME_SOURCE"
vagrant box list | grep $VAGRANT_BOX_NAME_SOURCE

LIST_GREP=`vagrant box list | grep "$VAGRANT_BOX_NAME_SOURCE"`
echo "$LIST_GREP"

if [ -n "$LIST_GREP" ]; then
  echo '---------------------------------------------------'
  echo "vagrant box remove $VAGRANT_BOX_NAME_SOURCE --force"
  vagrant box remove $VAGRANT_BOX_NAME_SOURCE --force
  wait
fi

# Remove inaccessible machines
for vvm in $(VBoxManage list vms | grep "inaccessible")
do
  vm_id=$(echo "${vvm}" | awk '{print substr($2, 2, length($2) - 2)}')
  vm_name=$(echo "${vvm}" | awk '{print substr($1, 2, length($1) - 2)}')
  VBoxManage unregistervm ${vm_name}
done

remove_virtual_machines () {
  DISTRO=$1
  SUITE=$2
  for vvm in $(VBoxManage list vms | grep "$VAGRANT_BOX_NAME_SOURCE")
  do
    # Virtual machine to remove: ${vvm}
    vm_id=$(echo "${vvm}" | awk '{print substr($2, 2, length($2) - 2)}')
    vm_name=$(echo "${vvm}" | awk '{print substr($1, 2, length($1) - 2)}')

    # Powering off: ${vm_id}
    VBoxManage controlvm ${vm_name} poweroff

    # Unregistering: ${vm_id}
    VBoxManage unregistervm ${vm_name}
  done
  for vm_dir in $(ls "/home/`whoami`/VirtualBox VMs/" | grep "$VIRTUALBOX_NAME")
  do
    echo '--------------'
    echo "rm -rf $vm_dir"
    rm -rf $vm_dir
  done
}

remove_virtual_machines $DISTRO $SUITE

rm -rf output-virtualbox-iso

if [[ -f $LOCAL_BOX_FILE_DEST ]] ; then
  rm $LOCAL_BOX_FILE_DEST
fi

VAGRANT_DIR="/home/`whoami`/.vagrant.d/boxes/$VAGRANT_BOX_NAME_SOURCE"
if [ -d "$VAGRANT_DIR" ]; then
  echo '-------------------'
  echo "rm -rf $VAGRANT_DIR"
  rm -rf $VAGRANT_DIR
fi

echo '************************************'
echo 'FINISHED: pre-build cleaning process'
echo '************************************'
