# Vagrant Debian Buster -Build

## Purpose
This repository is used for building and publishing the Debian Buster Vagrant boxes.

## Stages
The process of building the Vagrant box consists of several stages:
* Stage 0: This stage uses Packer to create a Vagrant box from the Debian Buster Netinstall ISO file.
* Stage 1: This stage uses the output of Stage 0 to provide the Vagrant box equivalent of the [Debian Buster - Minimal Stage 1](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage1/) Docker image.
* Stage 2: This stage uses the output of Stage 1 to provide the Vagrant box equivalent of the [Debian Buster - Minimal Stage 2](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2/) Docker image.
* Stage 3: This stage uses the output of Stage 2 to provide the Vagrant box equivalent of the [Debian Buster - Minimal RVM](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm/) Docker image.
* Stage 4A: This stage uses the output of Stage 3 to provide the Vagrant box equivalent of the [Debian Buster - RVM Rails General](https://gitlab.com/rubyonracetracks/docker-debian-buster-rvm-rails-general) Docker image.

## Origins
Stage 0 is a modified version of the [deimosfr/packer-debian](https://github.com/deimosfr/packer-debian) repository on GitHub.  Creating a repository from scratch to build a Vagrant box with Packer is a frustrating, time-consuming, and error-prone process.  It's best to start with an existing repository that works and gradually make modifications.  I picked the [deimosfr/packer-debian](https://github.com/deimosfr/packer-debian) repository because it is uses Debian and VirtualBox.

## Why is your lineup of published Vagrant boxes so limited?
* Publishing a Vagrant box file is a painfully long process that takes hours, because my home Internet connection has a very slow upload speed.
* The Stage 0, 1, 2, and 3 Vagrant boxes provide nothing that the downstream boxes don't provide, so I've decided not to publish them.
* Because the Vagrant boxes take so long to upload, the only Vagrant boxes I publish are the general-purpose Ruby on Rails box and those for selected projects that I cannot get working with Docker.  Thus, Vagrant is the development environment of last resort.

## Where are these Vagrant boxes published?
These Vagrant boxes are on [SourceForge](https://sourceforge.net/projects/vagrant-debian-buster/files/).

## Why do you publish your Vagrant boxes on SourceForge instead of Vagrant Cloud?
I was unable to figure out how to automate the process of releasing a new Vagrant box on Vagrant Cloud.  The procedures I found didn't work for me.  So I instead set up this repository to just replace the old Vagrant box with a new one on SourceForge.  As is the case with my Docker-publishing repositories, I use the "set -e" command in most of my scripts to automatically exit the build process if a process fails.  This safeguard ensures that a good Vagrant box does not get replaced with a defective one.

## Why don't you use continuous integration to publish this Vagrant box?
Vagrant and VirtualBox do not work inside continuous integration environments, which rely on Docker.  VirtualBox does not work within Docker.

## Why don't you use cloud services to publish this Vagrant box?
I was unable to build Vagrant boxes within Amazon AWS EC2 or Microsoft Azure's virtual machines.  This is not Amazon's fault or Microsoft's fault.  VirtualBox does not support 64-bit nesting.  While I can run a 32-bit virtual machine within a 64-bit virtual machine, I am unable to run a 64-bit virtual machine within a 64-bit virtual machine.

## Why don't you use bare metal services to publish this Vagrant box?
The bare metal services are FAR more expensive than using a local machine at home.
