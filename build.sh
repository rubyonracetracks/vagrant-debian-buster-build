#!/bin/bash

set -e

DATE=`date +%Y-%m%d-%H%M%S`

mkdir -p log
bash exec-build.sh 2>&1 | tee log/log-$DATE.txt
