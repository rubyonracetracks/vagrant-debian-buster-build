#!/bin/bash

set -e

ABBREV=$1

DISTRO='debian'
SUITE='buster'
BOX_NAME="$DISTRO-$SUITE-$ABBREV"
BOX_FILE="$BOX_NAME.box" # Output of build script

git pull origin master

# Upload a small file to SourceForge
DATE=`date +%Y-%m%d-%H%M%S`
mkdir -p log
echo "Testing upload - $BOX_NAME - $DATE" > test-upload.txt
echo '####################################################################################################'
echo "rsync -avPz -e ssh test-upload.txt jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE"
rsync -avPz -e ssh test-upload.txt jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE

time bash exec-build-$ABBREV.sh
echo ' /|\'
echo '/ | \'
echo '  |  '
echo "Time used to build $BOX_FILE"

echo '###################################################################################################'
echo "time rsync -avPz -e ssh $BOX_FILE jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE"
time rsync -avPz -e ssh $BOX_FILE jhsu802701@frs.sourceforge.net:/home/frs/p/vagrant-$DISTRO-$SUITE
echo ' /|\'
echo '/ | \'
echo '  |  '
echo "Time used to upload $BOX_FILE"
