#!/bin/bash

set -e

bash build-template-docker.sh 'rubyonracetracks' 'debian' 'buster' 'rvm-rails-general' 'min-rvm'
